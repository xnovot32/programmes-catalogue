Katalog je LaTeXová dokumentová třída pro sazbu studijních katalogů na
Masarykově univerzitě.

# Instalace

Pro instalaci balíčku interpretujte soubor `katalog.ins` pomocí TeXového stroje
s podporou kódování Unicode (např. LuaTeX):

``` bash
$ luatex katalog.ins
```

Po spuštění vzniknou soubory `katalog.cls`, `MUNI.fontspec`
a `SkolarLatin.fontspec`. Tyto soubory přesuňte spolu s adresářem
`system/` a se soubory `ukazka.tex` a `ukazka-poznamky.tex` do
pracovního adresáře, kde budete připravovat svůj dokument:

```
$ ls pracovní-adresar/
system/
ukazka.tex
ukazka-poznamky.tex
katalog.cls
MUNI.fontspec
SkolarLatin.fontspec
```

# Začlenění vlastních úprav do balíčku katalog

1. Stáhněte se repositář ze serveru GitLab FI do pracovní kopie na disku, kde
   budete provádět úpravy:

    ``` bash
    $ git clone https://gitlab.fi.muni.cz/external_relations/document_templates/programmes-catalogue.git
    ```

2. Vytvořte si ve svém lokálním Git repozitáři samostatnou pojmenovanou větev,
   ve které budete provádět úpravy:

    ``` bash
    $ cd programmes-catalogue
    $ git checkout -B oprava-strankovani
    ```

3. Pomocí svého oblíbeného textového editoru upravte soubor `katalog.dtx` a
   zaneste své úpravy jako revizi do své pracovní kopie:

    ``` bash
    $ git add katalog.dtx
    $ git commit -m 'Opravil/a jsem stránkování'
    ```

   Následně svou větev se změnami odešlete do repositáře na serveru GitLab FI:

    ``` bash
    $ git push origin oprava-strankovani
    ```

4. Na webu repositáře na serveru GitLab FI v záložce „Merge requests“ klikněte
   na tlačítko „New merge request“ a vytvořte požadavek na začlenění změn
   z Vaší větve `oprava-strankovani` do větve `main`. Správce repozitáře změny
   zkontroluje a potvrdí jejich začlenění do repozitáře.

# Vysázení ukázkového dokumentu

Vysázejte ukázkový dokument `ukazka.tex` pomocí nástroje [LaTeXMk][1]:

``` bash
$ latexmk -lualatex ukazka.tex
```

Po úspěšné sazbě vznikne soubor `draft.pdf` s obsahem ukázkového dokumentu.

 [1]: https://www.ctan.org/pkg/latexmk/ (latexmk – Fully automated LaTeX document generation)

# Další informace

Další informace o třídě katalog najdete v [dokumentaci třídy][2]. Dokumentaci
si můžete sami vysázet pomocí nástroje [LaTeXMk][1]:

``` bash
$ latexmk katalog.dtx
```

Po úspěšné sazbě vznikne soubor `katalog.pdf` s dokumentací.

 [2]: https://external_relations.pages.fi.muni.cz/document_templates/programmes-catalogue/katalog.pdf
